import { Component, OnInit } from '@angular/core';
import { Empleado } from '../empleados/empleado';
import { EmpleadoService } from '../empleados/empleado.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html'
})
export class RegistroComponent implements OnInit {

  private empleado: Empleado = new Empleado()

  constructor(private empleadoService: EmpleadoService, private router: Router) { }

  ngOnInit(): void {
  }

  public create(): void{
    this.empleadoService.create(this.empleado).subscribe(
      response => this.router.navigate(['/listar'])
    )
  }

}
