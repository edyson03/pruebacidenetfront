import { Injectable } from '@angular/core';
import { Empleado } from './empleado';
import { Observable, of } from "rxjs";
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  private urlEndPointListar: string = 'http://localhost:8080/empleado/listar';
  private urlEndPointRegistrar: string = 'http://localhost:8080/empleado/registrar';

  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'})
  
  constructor(private http: HttpClient) { }

  getEmpleados(): Observable<Empleado[]>{
    return this.http.get<Empleado[]>(this.urlEndPointListar);
  }

  create(empleado: Empleado) : Observable<Empleado>{
    return this.http.post<Empleado>(this.urlEndPointRegistrar, empleado, {headers: this.httpHeaders});
  }
}
