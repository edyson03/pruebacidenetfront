export class Empleado {

    id: number;
	nombre: String;
	otroNombre: String;
	primerApellido: String;
	segundoApellido: String;
	pais: number;
	tipoIdentificacion: number;
	numeroIdentificacion: String;
	correo: String;
	fechaHoraIngreso: String;
	area: number;
	estado: String;
}
