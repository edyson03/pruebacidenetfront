import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { EmpleadosComponent } from './empleados/empleados.component';
import { EmpleadoService } from './empleados/empleado.service';
import { RouterModule, Routes} from "@angular/router"
import { HttpClientModule} from '@angular/common/http';
import { RegistroComponent } from './registro/registro.component';

const routes: Routes = [
  { path: '', redirectTo: './empleado', pathMatch: 'full'} ,
  { path: 'empleado', component: EmpleadosComponent}, 
  { path: 'registro', component: RegistroComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    EmpleadosComponent,
    RegistroComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [EmpleadoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
